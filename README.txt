Copyright 2017-2018 http://www.novatree.com

Description
-----------
The wallet module brings wallet functionality to drupal.

Admin can add currencies like USD, EURO etc.

Admin can then create categories for the given currencies like general etc.

A block displays the balance of the user.


Installation
------------
To install this module, do the following:

1. Extract the tar ball that you downloaded from Drupal.org.

2. Upload the wallet directory and all its contents to your
   modules directory.

Configuration
-------------
To enable this module do the following:

1. Go to Admin -> Modules, and enable wallet.
   Check the messages to make sure that you did not get any errors
   on database creation.

2. Go to Admin -> Settings -> Wallet Listing.

   You can see the wallet currency, category and transaction listings here.

3. Go to Admin -> Structure -> Wallet Settings.

    You can configure wallet related field settings here.


Bugs/Features/Patches:
----------------------
If you want to report bugs, feature requests, or submit a patch, please do so
at the project page on the Drupal web site.
http://drupal.org/project/wallet

Author
------
Amit Maity (amit@novatree.com)
Manish Tapadar (manish@novatree.com)

If you use this module, find it useful, and want to send the author
a thank you note, then drop an email at mentioned adresses.

The author can also be contacted for paid customizations of this
and other modules.
